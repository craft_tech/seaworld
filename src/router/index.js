import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

let router = new Router({
  routes: [
     {
      path: '/',
      name: 'HelloWorld',
      component: resolve => {
        require(['../components/index.vue'], resolve)
    }, //懒加载
    },
    {
      path: '/loading',
      name: 'loading',
      component: resolve => {
          require(['../components/home/loading.vue'], resolve)
      }, //懒加载
    },
    {
      path: '/home',
      name: 'home',
      // redirect:'/',
      component: resolve => {
        require(['../components/home/home.vue'], resolve)
      }, //懒加载
    },
    {
      path: '/video',
      name: 'video',
      component: resolve => {
          require(['@/components/video/video.vue'], resolve)
      }, //懒加载
    },{
      path: '/video1',
      name: 'video1',
      component: resolve => {
          require(['@/components/video/video1.vue'], resolve)
      }, //懒加载
    },
    {
      path: '/team',
      component: resolve => {
        require(['@/components/team/index.vue'], resolve)
      },
      children:[
        {
          path: '/team/choice',
          name: '战队选择',
          component: resolve => {
            require(['@/components/team/choice.vue'], resolve)
          }
        },
        {
          path: '/team/details/:ids',
          name: '战队详情',
          component: resolve => {
            require(['@/components/team/details.vue'], resolve)
          }
        },
        {
          path: '/team/join/:ids',
          name: '战队详情已加入',
          component: resolve => {
            require(['@/components/team/join.vue'], resolve)
          }
        },
        {
          path: '/team/member',
          name: '召唤战友',
          component: resolve => {
            require(['@/components/team/member.vue'], resolve)
          }
        },
        {
          path: '/team/task',
          name: '查看任务',
          component: resolve => {
            require(['@/components/team/task.vue'], resolve)
          }
        },
        {
          path: '/team/taskdetails/:taskid',
          name: '任务详情',
          component: resolve => {
            require(['@/components/team/taskdetails.vue'], resolve)
          }
        }
      ]
    },
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.length ===0) {  //如果未匹配到路由
    from.path? next({ path:from.path}) : next('/');   //如果上级也未匹配到路由则跳转主页面，如果上级能匹配到则转上级路由
  } else {
    next();    //如果匹配到正确跳转
  }
});

export default router
