import  axios from  'axios'
import  qs from  'qs'
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';

//用户授权
export function getOauth() {
    return axios.get('https://cfow-q3.gypserver.com/OceanQueue/Oauth',{
            params: {
                redirect_url: 'https://cfow-q3.gypserver.com/index.html'
            }
        })
}

//查询组队详情
export function getTeam(param) {
    return axios.post('https://cfow-q3.gypserver.com/OceanQueue/GetTeam',
    qs.stringify({openId:param}))
}

//加入战队
export function joinTeam(param) {
    return axios.post('https://cfow-q3.gypserver.com/OceanQueue/JoinTeam',qs.stringify(param))
}

