// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import YDUI from 'vue-ydui'; /* 相当于import YDUI from 'vue-ydui/ydui.rem.js' */
import 'vue-ydui/dist/ydui.rem.css';
import wxapis from "./libs/wxapi.js"
/* 使用px：import 'vue-ydui/dist/ydui.px.css'; */
Vue.use(YDUI);

Vue.config.productionTip = false

let ua = navigator.userAgent.toLowerCase();
//系统判断
if(ua.match(/iPhone/i) == "iphone") {
  //iphone
  Vue.prototype._equipment = 'iphone'

}else if(ua.match(/Android/i) == "android"){
  //android
  Vue.prototype._equipment = 'android'
}else if(ua.match(/MicroMessenger/i)=="micromessenger") {
  //是微信
  Vue.prototype._equipment = 'wx'
}


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
    data(){
    return{
      bgmiconUrl:'https://cfow-q3.gypserver.com/static/bgmicon.png',
      isplay:false
    }
  }
})
