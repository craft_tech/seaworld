import wx from 'weixin-js-sdk'

function loadJs(url,callback){
    var script=document.createElement('script');
    script.type="text/javascript";
    if(typeof(callback)!="undefined"){
    if(script.readyState){
    script.onreadystatechange=function(){
      if(script.readyState == "loaded" || script.readyState == "complete"){
      script.onreadystatechange=null;
      callback();
      }
    }
    }else{
      script.onload=function(){
        callback();
      }
    }
  }
    script.src=url;
    document.body.appendChild(script);
}
// console.log('wx_conf' + JSON.stringify(wx_conf))
// wx.config({
//   debug: false,
//   appId: wx_conf.appId,
//   timestamp: wx_conf.timestamp,
//   nonceStr: wx_conf.nonceStr,
//   signature: wx_conf.signature,
//   jsApiList: [
//     'checkJsApi',
//     'onMenuShareTimeline',
//     'onMenuShareAppMessage'
//   ]
// });


var shareData = {
  title: '',
  desc: '',
  link: '',
  imgUrl: '',
  type:'',
  dataUrl:'',
  success: function() {
    console.log('-----success'+ shareData.link)
  },
  error: function() {
  },
  cancel: function () { 
      // 用户取消分享后执行的回调函数
  }
} 
//默认分享
// loadJs("//demo2.gypserver.com/weixinShareFile/?url=" + encodeURIComponent(location.href.split('#')[0]),function(){
//   console.log('wx_conf' + JSON.stringify(wx_conf))
//   wx.config({
//       debug: false,
//       appId: wx_conf.appId,
//       timestamp: wx_conf.timestamp,
//       nonceStr: wx_conf.nonceStr,
//       signature: wx_conf.signature,
//       jsApiList: [
//           'checkJsApi',
//           'onMenuShareTimeline',
//           'onMenuShareAppMessage'
//       ]
//     }); 
//   //无战队情况下分享  
//   wx.ready(function() {
//     //分享标题
//     shareData.title = '保卫海洋站出来';
//     //分享内容
//     shareData.desc = '海洋保护战队有礼召集！快来组建你的队伍吧';
//     //分享网址
//     shareData.link = 'https://cfow-q3.gypserver.com';
//     //分享图片
//     shareData.imgUrl = 'https://cfow-q3.gypserver.com/static/share1.jpg';
//     console.log('wu战队分享---5' +  JSON.stringify(shareData))
//     wx.onMenuShareAppMessage(shareData);
//     wx.onMenuShareTimeline(shareData);    
//   });   

// });

//有战队分享  encodeURIComponent(location.href.split('#')[0])
export function wxshare(ids,teamid){
    loadJs("//demo2.gypserver.com/weixinShareFile/?url=" + encodeURIComponent(location.href.split('#')[0]),function(){
      // console.log('wx_conf' + JSON.stringify(wx_conf))
      wx.config({
        debug: false,
        appId: wx_conf.appId,
        timestamp: wx_conf.timestamp,
        nonceStr: wx_conf.nonceStr,
        signature: wx_conf.signature,
        jsApiList: [
            'checkJsApi',
            'onMenuShareTimeline',
            'onMenuShareAppMessage'
        ]
      });   
    });
      wx.ready(function() {
        if(teamid){
          //分享标题
          shareData.title = '保卫海洋站出来';
          //分享内容
          if(ids == 1){
            shareData.desc = '寻找海洋勇士！拿出勇气，和我加入战队赢大礼吧';
             //分享图片
            shareData.imgUrl = 'https://cfow-q3.gypserver.com/static/share_1.jpeg';
          }else if(ids == 2){
            shareData.desc = '寻找海洋英雄！坚守正义，和我加入战队赢大礼吧';
             //分享图片
            shareData.imgUrl = 'https://cfow-q3.gypserver.com/static/share_2.jpeg';
          }else if(ids == 3){
            shareData.desc = '寻找海洋英雄！激发智慧，和我加入战队赢大礼吧';
             //分享图片
            shareData.imgUrl = 'https://cfow-q3.gypserver.com/static/share_3.jpeg';
          }
          //分享网址
          shareData.link = `https://cfow-q3.gypserver.com?ids=${ids}&teamid=${teamid}`;
         
        }else{
           //分享标题
          shareData.title = '保卫海洋站出来';
          //分享内容
          shareData.desc = '海洋保护战队有礼召集！快来组建你的队伍吧';
          //分享网址
          shareData.link = 'https://cfow-q3.gypserver.com';
          //分享图片
          shareData.imgUrl = 'https://cfow-q3.gypserver.com/static/share.jpg';
        }
        // console.log('wxshare' + JSON.stringify(shareData))
        wx.onMenuShareAppMessage(shareData);
        wx.onMenuShareTimeline(shareData);    
      });  
      wx.error(function(res){
        console.log('出错了'+JSON.stringify(res))
        loadJs("//demo2.gypserver.com/weixinShareFile/?url=" + encodeURIComponent(location.href.split('#')[0]),function(){
          // console.log('wx_conf' + JSON.stringify(wx_conf))
          wx.config({
            debug: false,
            appId: wx_conf.appId,
            timestamp: wx_conf.timestamp,
            nonceStr: wx_conf.nonceStr,
            signature: wx_conf.signature,
            jsApiList: [
                'checkJsApi',
                'onMenuShareTimeline',
                'onMenuShareAppMessage'
            ]
          });   
        });
      });
      
}





